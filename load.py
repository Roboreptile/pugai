import tensorflow as tf
import PIL
import numpy as np
import matplotlib.pyplot as plt

from const import*

decision = input("VGG mode? (Y/N): ")
if not decision=="Y":
    from src.const import *
else:
    from vgg.const import*

print("Available models: ") 
dir = os.listdir(GENERATORS_DIR)
print(dir)

gen = input("Provide full name of the generator (without extension) (empty for last): ")

if(len(gen)==0):
    gen = dir[-1]
else:
    gen+=".h5"

print("Loading "+gen+" ...")
model = tf.keras.models.load_model(GENERATORS_DIR+"/"+gen)
model.summary()
img = input("Provide image name (without extension) (empty for input.png): ")

if(len(img)==0):
    img = "input.png"
else:
    img += ".h5"

print("Loading "+img+"...")
img = PIL.Image.open(img)
img = img.resize((IMAGE_SIZE,IMAGE_SIZE),PIL.Image.ANTIALIAS)
img = np.array(img,dtype=np.float32)
img = img/255;
img = np.reshape(img, (1,IMAGE_SIZE,IMAGE_SIZE,3))
predictions = model.predict(img)

fig = plt.figure()
plt.imshow(predictions[0, :, :, :])
plt.savefig("output.png")
print("Done. Check output.png!")