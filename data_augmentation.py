
import os
import xml.etree.ElementTree as et
import numpy as np
import cv2


#specify paths before use
path_img = './data_training/Images/'
path_off = './data_training/Annotation/'
path_dest = './data_training/AugmentedImages_'

path_img_pug = './data_training/ImagesPug/'
path_off_pug = './data_training/AnnotationPug/'
path_dest_pug = './data_training/AugmentedImagesPug_'

width = int(input("Image size: "))
height = width
# ==============================================

images = []
offsets = []
changesize = True

blurparams  = (2, 2)
path_dest += str(width) + "/"
path_dest_pug += str(width) + "/"

try:
    os.mkdir(path_dest)
    os.mkdir(path_dest_pug)
except:
    print("These files were already generated. Continue? Y/N")
    decision = input()
    if not decision == "Y":
        exit(1)

print("This might take a while (up to 10 minutes)...")
for root, dirs, files in os.walk(path_img):
	for file in files:
		images.append(os.path.join(root,file))

for root, dirs, files in os.walk(path_off):
	for file in files:
		offsets.append(os.path.join(root,file))

print(len(images))
for i in range(len(images)) :
    
    tree=et.parse(offsets[i])
    root = tree.getroot()

    for time in root.iter('xmax'):
        xmax = time.text

    for time in root.iter('xmin'):
        xmin = time.text

    for time in root.iter('ymax'):
        ymax = time.text

    for time in root.iter('ymin'):
        ymin = time.text

    img = cv2.imread(images[i])
    
    for j in range (-10, 10, 5) :
        if int(xmin)+j > 0 and int(ymin)+j > 0 and  int(ymax) + j < img.shape[0] and int(xmax) + j < img.shape[1] :
            roi = img[int(ymin)+j:int(ymax)+j, int(xmin)+j:int(xmax)+j]
            if changesize == True :
                roi = cv2.resize(roi, (width,height), interpolation = cv2.INTER_AREA)

            cv2.imwrite(path_dest + 'dog' + str(i) +'ver' + str(j) + '.png', roi)
            # flip x axis
            flip = cv2.flip(roi,1)
            cv2.imwrite(path_dest + 'dog' + str(i) + 'ver' + str(j) + 'ver_flipped_x.png', flip)
            #blur image
            image = cv2.blur(roi, blurparams) 
            cv2.imwrite(path_dest + 'dog' + str(i) + 'ver' + str(j) + 'ver_blurry.png', flip)

path_img = path_img_pug
path_off = path_off_pug
path_dest = path_dest_pug

images = []
offsets = []

for root, dirs, files in os.walk(path_img):
	for file in files:
		images.append(os.path.join(root,file))

for root, dirs, files in os.walk(path_off):
	for file in files:
		offsets.append(os.path.join(root,file))

for i in range(len(images)) :
    
    tree=et.parse(offsets[i])
    root = tree.getroot()

    for time in root.iter('xmax'):
        xmax = time.text

    for time in root.iter('xmin'):
        xmin = time.text

    for time in root.iter('ymax'):
        ymax = time.text

    for time in root.iter('ymin'):
        ymin = time.text

    img = cv2.imread(images[i])
    
    for j in range (-10, 10, 5) :
        if int(xmin)+j > 0 and int(ymin)+j > 0 and  int(ymax) + j < img.shape[0] and int(xmax) + j < img.shape[1] :
            roi = img[int(ymin)+j:int(ymax)+j, int(xmin)+j:int(xmax)+j]
            if changesize == True :
                roi = cv2.resize(roi, (width,height), interpolation = cv2.INTER_AREA)
            cv2.imwrite(path_dest + 'dog' + str(i) +'ver' + str(j) + '.png', roi)
            # flip x axis
            flip = cv2.flip(roi,1)
            cv2.imwrite(path_dest + 'dog' + str(i) + 'ver' + str(j) + 'ver_flipped_x.png', flip)
            #blur image
            image = cv2.blur(roi, blurparams) 
            cv2.imwrite(path_dest + 'dog' + str(i) + 'ver' + str(j) + 'ver_blurry.png', flip)
    



