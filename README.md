# PugAI - Style Transfer Generative Adversarial Network for the cute pugs

## Project Description

Use dogs of different breeds and chage their breed into a PUG! ❤️
By using the GAN network/ Style Transfer GAN network one can generate/transfer the style of a pug onto any dog input image (or any image in that regard)

![Results so far](demos/output.png)

- [X] Uses custom generator/discriminator models for GAN (without Style Transfer)
- [X] Uses VGG19 network for the style transfer (automatic download with tensorflow)
- [X] Code cleanup
- [ ] Improve generated results [Currently in progress]
- [ ] Custom model style transfer without the need for VGG19

## Before Running:

1. Download the following dataset:
   [Stanford Dogs Dataset](https://www.kaggle.com/jessicali9530/stanford-dogs-dataset)
2. Extract the folders: *Images* from *images* directory and *Annotation* from *annotations*
3. From these directories, search for images folder containing pug images, and annotations folder containing pug annotations. Extract these folders, and rename them to *ImagesPug* and *AnnotationPug*
4. Put all 4 extracted folders inside the *data_training* directory
5. In the command line, navigate to the folder directory and run (preferably on a fresh python environment):
   Windows:
   `python -m pip install -r requirements.txt`
   Linux/Mac:
   `python3 -m pip install -r requirements.txt`

## Data Augmentation

Due to the limited amount of pug images in the dataset, solution contains a basic data augmentator implementation.
After performing the steps from the 'Before Running' section, launch *data_augmentation.py* :

`python data_augmentation.py`

This program creates multiple images by utilizing blur, image flip, and image x- and y- offsetting. In addition, it usees the annotations to cut the dogs' images from the source images, reducing the backgorund noise.

## Solution Description

There are two main scripts provided.

- *launch_cnn.py* uses a custom model [CNN layers -> CNN Transpose layers] (one of a couple defined in the *src/generator/generator_models.py* ) to generate an image, and a smaller CNN network as a discriminator (models defined in *src/discriminator/discriminator_models.py*)
- *launch vnn.py* uses a VGG19 as a generator and uses an alterenative approach - by using transfer learning it applies the style/content loss on the VGG19 model with trained CNN layers in an attempt to transfer one dog style to another. The implementation of the GAN with this model as a generator can be found inside *vgg* folder

On launch, a couple of example files are created inside *data_output* directory:

- *examples_input.png* shows a random selection of 100 dogs used to train the generator
- *exmples_output.png* shows a random selection of 100 pugs used as training targets
- *generator_test.png* shows the result of appplying the model onto a random dog image before training

Both methods' training outcomes are saved as follows:

- Models are saved every 10 epochs to *saved_models* folder
- Training checkpoints are saved every 2 epochs to *saved_checkpoints* folder
- After every epoch, two test images are created - one from a random set of dogs, and another from noise. They are saved into *data_output/doggy* and *data_output/noisy* folders respectively.

After the training phase ends:

- Two GIF files are saved into *data_output* directory, that contain all the images from *doggy* and *noisy* folders. These images show how the network's output was chaning every epoch.
- A loss/epoch graph is saved as *graphy.png*.

## Current Results

The best result achieved so far was using the VGG network

![Result from the VGG network](demos/output.png)

CNN method so far has managed to create some beautiful intros to James Bond movies with splashes of dogs mixed in-between

[GIF loading takes a while!]

GIF from doggos:

![GIF from doggos](demos/gif_doggy.gif)

GIF from noise:

![GIF from noise](demos/gif_noisy.gif)
