if(True):
    from src.data_provider import DataProvider, DataType
    from src.generator_provider import GeneratorProvider, GeneratorType
    from src.discriminator_provider import DiscriminatorProvider, DiscriminatorType
    from src.gan_provider import GANProvider
else:
    from vgg.data_provider import DataProvider, DataType
    from vgg.generator_provider import GeneratorProvider, GeneratorType
    from vgg.discriminator_provider import DiscriminatorProvider, DiscriminatorType
    from vgg.gan_provider import GANProvider
    
data_provider = DataProvider(
    data_in = DataType.SUPER_ALL,
    data_out = DataType.SUPER_PUG, 
    reduce_images_in = 100,
    reduce_images_out = 1,
    set_batch = 0,
    set_buffer = 0,
    cheat = False
)

generator_provider = GeneratorProvider(GeneratorType.V1)
discriminator_provider = DiscriminatorProvider(DiscriminatorType.V3)

gan_provider = GANProvider(
    generator_provider, 
    discriminator_provider, 
    data_provider
)

gan_provider.load_data()
gan_provider.train()
gan_provider.make_gif()