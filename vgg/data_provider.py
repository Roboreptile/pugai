from numpy.core.shape_base import block
from pandas import cut
import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
import os
import PIL
from random import randint

from enum import Enum

from src_vgg.const import *


class DataType(Enum):
    ALL = 1
    PUG = 2
    NOISE = 3
    SUPER_ALL = 4
    SUPER_PUG = 5
    
class DataProvider:
    data_type_in:DataType
    data_type_out:DataType

    dataset_in:tf.data.Dataset
    dataset_out:tf.data.Dataset
    
    def __init__(self, data_in:DataType, data_out:DataType, reduce_images_in = 1, reduce_images_out = 1, reduce_batch = 1, cheat=False):
        self.data_type_in = data_in
        self.data_type_out = data_out

        self.cut_in = reduce_images_in
        self.cut_out = reduce_images_out
        self.batch_cut = reduce_batch
        self.cheat = cheat
    

    def load_data(self):
        data_in:np.array
        
        print("Input images - ",end='')

        if(self.data_type_in is DataType.SUPER_ALL):
            data_in = self._load_super_all_data(self.cut_in)
        elif(self.data_type_in is DataType.SUPER_PUG):
            data_in = self._load_super_pug_data(self.cut_in)  
        else:
            data_in = self._load_noise_data()
        
        self._show_example_data(data_in,'input')

        data_out:np.array
        print("Output images - ",end='')
        if(self.data_type_out is DataType.SUPER_ALL):
            data_out = self._load_super_all_data(self.cut_out) 
        elif(self.data_type_out is DataType.SUPER_PUG):
            data_out = self._load_super_pug_data(self.cut_out)  
        else:
            data_out = self._load_noise_data()

        self._show_example_data(data_in,'input')
        self._show_example_data(data_out,'output')
        print("Example images are now available.")

        train_images_in = (data_in - 127.5)/127.5
        train_images_out =(data_out - 127.5)/127.5
        self.dataset_in = tf.data.Dataset.from_tensor_slices(train_images_in ).shuffle(1809, reshuffle_each_iteration=True).batch(1, drop_remainder=True)
        self.dataset_out= tf.data.Dataset.from_tensor_slices(train_images_out).shuffle(1809,reshuffle_each_iteration=True).batch(1, drop_remainder=True)
        
        example_dogs = train_images_in[np.random.choice(train_images_in.shape[0], GIF_SIZE_SQUARED, replace=True)]
        example_pugs = train_images_out[np.random.choice(train_images_out.shape[0], GIF_SIZE_SQUARED, replace=True)]

        print("Batches per epoch:" + str(len(self.dataset_in)))
        return self.dataset_in, self.dataset_out, example_dogs, example_pugs
    
    def _show_example_data(self,data, type):
        fig = plt.figure(figsize=(EXAMPLES_SIZE, EXAMPLES_SIZE))

        for i in range(EXAMPLES_SIZE_SQUARED):
            plt.subplot(EXAMPLES_SIZE, EXAMPLES_SIZE, i+1)
            plt.imshow(data[randint(0,data.shape[0]-1), :, :, :]/255)
            plt.axis('off')

        plt.savefig('./'+EXAMPLES_NAME+'_'+type+'.png')
        plt.close(fig)
            

    def _load_super_all_data(self,cut):
        directory = os.listdir(SUPER_DIR)
        size = len(directory)
        size = size//cut
        output = np.zeros((size+1,IMAGE_SIZE,IMAGE_SIZE,3),dtype=np.float32)

        print("Loading started...")
        for iterator, pug in enumerate(directory):
            img = PIL.Image.open(SUPER_DIR+pug)
            scaled_img = img.resize((IMAGE_SIZE,IMAGE_SIZE),PIL.Image.ANTIALIAS)
            output[iterator,:,:,:] = np.array(scaled_img,dtype=np.float32)
            if(iterator==size): break
        print(str(size)+" super images loaded!")
        
        return output

    def _load_super_pug_data(self,cut):
        directory = os.listdir(SUPER_PUG_DIR)
        size = len(directory)
        size = size//cut
        output = np.zeros((size+1,IMAGE_SIZE,IMAGE_SIZE,3),dtype=np.float32)

        
        print("Loading started...")
        for iterator, pug in enumerate(directory):
            img = PIL.Image.open(SUPER_PUG_DIR+pug)
            scaled_img = img.resize((IMAGE_SIZE,IMAGE_SIZE),PIL.Image.ANTIALIAS)
            output[iterator,:,:,:] = np.array(scaled_img,dtype=np.float32)
            if(iterator==size): break
        print(str(size)+" super pug images loaded!")
        
        return output