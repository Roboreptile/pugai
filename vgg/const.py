import os

ALL_DIR = "./training_data/Images/"
PUG_DIR = "./training_data/Images/n02110958-pug/"
SUPER_DIR = "./training_data/ImagesV2/"
SUPER_PUG_DIR = "./training_data/ImagesV2Pug/"
SUPER_DIR = "./training_data/ImagesV2224/"
SUPER_PUG_DIR = "./training_data/ImagesV2Pug224/"
CHECKPOINT_DIR = './training_checkpoints'
GENERATORS_DIR = './gens'

IMAGE_SIZE = 224

BATCH_SIZE = 1
BUFFER_SIZE = 1809 #Note: 167268 / 1809
EPOCHS = 1000
SAVE_EVERY_X_EPOCHS = 25
VGG_REPEAT = 10

EXAMPLES_SIZE = 10
EXAMPLES_NAME = "examples"
GIF_SIZE = 4
GIF_NAME = "giffy"

GIF_SIZE_SQUARED = GIF_SIZE*GIF_SIZE
EXAMPLES_SIZE_SQUARED = EXAMPLES_SIZE*EXAMPLES_SIZE
CHECKPOINT_PREFIX = os.path.join(CHECKPOINT_DIR, "ckpt")
GENERATOR_PREFIX = os.path.join(GENERATORS_DIR, "genVGG-")