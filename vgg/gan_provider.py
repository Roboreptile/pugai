
import tensorflow as tf
import glob
import imageio
import matplotlib.pyplot as plt
import time

from re import I
from tensorflow.python.data.ops.dataset_ops import BatchDataset


from src_vgg.const import *
from src_vgg.data_provider import *
from src_vgg.discriminator_provider import *
from src_vgg.generator_provider import *

class GANProvider:
    
    generator_provider:GeneratorProvider
    discriminator_provider:DiscriminatorProvider
    data_provider:DataProvider
    
    generator_model:tf.keras.models.Model
    discriminator_model:tf.keras.Sequential
    checkpoint:tf.train.Checkpoint
    
    data_in: BatchDataset
    data_out: BatchDataset
    
    seed_noise = tf.random.normal([GIF_SIZE_SQUARED, IMAGE_SIZE*IMAGE_SIZE*3])
    seed_dogs = tf.random.normal([GIF_SIZE_SQUARED, IMAGE_SIZE*IMAGE_SIZE*3])
    seed_pugs = tf.random.normal([GIF_SIZE_SQUARED, IMAGE_SIZE*IMAGE_SIZE*3])

    total_variation_weight=30

    def __init__(self,generator:GeneratorProvider,discriminator:DiscriminatorProvider, data:DataProvider):
        self.generator_provider = generator
        self.discriminator_provider = discriminator
        self.data_provider = data
        
        self.generator_model = generator.get()
        self.discriminator_model = discriminator.get()

    
    def load_data(self):
        s = time.time()
        self.data_in, self.data_out, self.seed_dogs, self.seed_pugs = self.data_provider.load_data()
        s = time.time() - s
        print("Total time to load images: "+str(round(s,2))+" seconds ("+str(round(s/60,2))+" minutes)")
        

    def train(self):
        print("Training started.")

        for epoch in range(EPOCHS):
            start = time.time()

            for image_batch_in, image_batch_out in zip(self.data_in,self.data_out):

                image_batch_in_var = tf.Variable(image_batch_in)
                image_batch_out_var = tf.Variable(image_batch_out)
                
                print()
                for r in range(VGG_REPEAT):
                    self._vgg_train_step(image_batch_in_var,image_batch_out_var)
                    print(".",end='',flugh=True)

                self._train_step(image_batch_in_var,image_batch_out)

            self._generate_and_save_images(epoch + 1)

            if (epoch + 1) % SAVE_EVERY_X_EPOCHS == 0:

                self.generator_model.save(GENERATOR_PREFIX+str(epoch+1)+".h5")
                print("Generator saved as: "+GENERATOR_PREFIX+str(epoch+1))

            print ('Time for epoch {} is {} sec'.format(epoch + 1, time.time()-start))

        self._generate_and_save_images(EPOCHS)
        self.generator_model.save(GENERATOR_PREFIX+"final.h5")

    def make_gif(self):
        anim_file = GIF_NAME+"_noisy.gif"
        with imageio.get_writer(anim_file, mode='I') as writer:
            filenames = glob.glob('./test/noisy/image*.png')
            filenames = sorted(filenames)
            for filename in filenames:
                image = imageio.imread(filename)
                writer.append_data(image)

        anim_file = GIF_NAME+"_doggy.gif"       
        with imageio.get_writer(anim_file, mode='I') as writer:
            filenames = glob.glob('./test/doggy/image*.png')
            filenames = sorted(filenames)
            for filename in filenames:
                image = imageio.imread(filename)
                writer.append_data(image)

    
    def clip_0_1(self,image):
        return tf.clip_by_value(image, clip_value_min=0.0, clip_value_max=1.0)

    @tf.function() 
    def _vgg_train_step(self,i_in:tf.Variable,i_out:tf.Variable):
        with tf.GradientTape() as gen_tape:
            outputs = self.generator_model(i_in)
            gen_loss = self.generator_provider.loss(i_in,i_out,outputs)
            gen_loss += self.total_variation_weight*tf.image.total_variation(i_in)
            gradients_of_generator = gen_tape.gradient(gen_loss, self.generator_model.trainable_variables)

        self.generator_provider.apply_gradients(gradients_of_generator,i_in)

        i_in.assign(self.clip_0_1(i_in))

    @tf.function()
    def _train_step(self,i_in:tf.Variable,i_out:tf.Variable):
        with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape:
            outputs = self.generator_model(i_in)

            real_output = self.discriminator_model(i_out, training=True)
            fake_output = self.discriminator_model(i_in, training=True)

            gen_loss = self.generator_provider.loss(i_in,i_out,outputs)
            gen_loss += self.total_variation_weight*tf.image.total_variation(i_in)
            disc_loss = self.discriminator_provider.loss(real_output, fake_output)

            gradients_of_generator = gen_tape.gradient(gen_loss, self.generator_model.trainable_variables)
            gradients_of_discriminator = disc_tape.gradient(disc_loss, self.discriminator_model.trainable_variables)

        self.generator_provider.apply_gradients(gradients_of_generator,i_in)
        self.discriminator_provider.apply_gradients(gradients_of_discriminator)

    def _generate_and_save_images(self, epoch):

        inp = tf.Variable(self.seed_noise)
        out = tf.Variable(self.seed_pugs)
        outputs = self.generator_model(inp)

        with tf.GradientTape() as gen_tape:   
            gen_loss = self.generator_provider.loss(inp,out,outputs)
            gen_loss += self.total_variation_weight*tf.image.total_variation(inp)
            gradients_of_generator = gen_tape.gradient(gen_loss, self.generator_model.trainable_variables)
        self.generator_provider.apply_gradients(gradients_of_generator,inp)
        inp.assign(self.clip_0_1(inp))

        fig = plt.figure(figsize=(4, 4))

        for i in range(inp.shape[0]):
            plt.subplot(4, 4, i+1)
            plt.imshow((inp[i, :, :, :]+1)/2)
            plt.axis('off')

        plt.savefig('./test/noisy/image_at_epoch_{:04d}.png'.format(epoch))
        plt.close(fig)

        inp = tf.Variable(self.seed_dogs)
        out = tf.Variable(self.seed_pugs)
        outputs = self.generator_model(inp)

        with tf.GradientTape() as gen_tape:   
            gen_loss = self.generator_provider.loss(inp,out,outputs)
            gen_loss += self.total_variation_weight*tf.image.total_variation(inp)
            gradients_of_generator = gen_tape.gradient(gen_loss, self.generator_model.trainable_variables)
        self.generator_provider.apply_gradients(gradients_of_generator,inp)
        inp.assign(self.clip_0_1(inp))

        fig = plt.figure(figsize=(4, 4))

        for i in range(inp.shape[0]):
            plt.subplot(4, 4, i+1)
            plt.imshow((inp[i, :, :, :]+1)/2)
            plt.axis('off')


        plt.savefig('./test/doggy/image_at_epoch_{:04d}.png'.format(epoch))
        plt.close(fig)
