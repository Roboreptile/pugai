from re import M
import tensorflow as tf

from enum import Enum

from src_vgg.const import *
from src_vgg.vgg import *

class GeneratorType(Enum):
    MOCK = 1
    SMALL = 2
    MEDIUM = 3
    BIG = 4
    HUGE = 5
    FINAL = 6
    VGG = 7

class GeneratorProvider:
    
    extractor:tf.keras.models.Model
    opt = tf.optimizers.Adam(learning_rate=0.02, beta_1=0.99, epsilon=1e-1)

    style_weight=1e-2
    content_weight=1e4


    def __init__(self,type:GeneratorType):
        self._initialize_generator()
    
    def loss(self, dog, pug, outputs):

        self.style_targets = self.extractor(dog)['style']
        self.content_targets = self.extractor(pug)['content']

        style_outputs = outputs['style']
        content_outputs = outputs['content']
        style_loss = tf.add_n([tf.reduce_mean((style_outputs[name]-self.style_targets[name])**2) 
                            for name in style_outputs.keys()])
        style_loss *= self.style_weight / self.num_style_layers

        content_loss = tf.add_n([tf.reduce_mean((content_outputs[name]-self.content_targets[name])**2) 
                                for name in content_outputs.keys()])
        content_loss *= self.content_weight / self.num_content_layers
        loss = style_loss + content_loss
        return loss
    
    def get(self):
        return self.extractor
    
    def apply_gradients(self,grad,image):
        self.opt.apply_gradients([(grad, image)])
        
    
    def _initialize_generator(self):
        content_layers = ['block5_conv2'] 

        style_layers = ['block1_conv1',
                        'block2_conv1',
                        'block3_conv1', 
                        'block4_conv1', 
                        'block5_conv1']

        content_layers = ['block5_conv2'] 

        style_layers = ['block1_conv1',
                        'block2_conv1',
                        'block3_conv1', 
                        'block4_conv1', 
                        'block5_conv1']
        
        self.num_content_layers = len(content_layers)
        self.num_style_layers = len(style_layers)

        self.extractor = StyleContentModel(style_layers, content_layers)
    
        

    