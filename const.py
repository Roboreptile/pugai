ALL_DIR              = "./training_data/Images/"
PUG_DIR              = "./training_data/Images/n02110958-pug/"
SUPER_DIR_PREFIX     = "./training_data/AugmentedImgs_"
SUPER_PUG_DIR_PREFIX = "./training_data/AugmentedPugs_"

CHECKPOINT_DIR       = './saved_checkpoints/'
GENERATORS_DIR       = './saved_models/'

EXAMPLES_NAME        = "example"
GIF_NAME             = "animated"
EXAMPLES_SIZE        = 10
GIF_SIZE             = 4

SUPER_DIR = SUPER_DIR_PREFIX + str(IMAGE_SIZE)+ "/"
SUPER_PUG_DIR += SUPER__PUG_DIR+ str(IMAGE_SIZE)+ "/"
GIF_SIZE_SQUARED = GIF_SIZE*GIF_SIZE
EXAMPLES_SIZE_SQUARED = EXAMPLES_SIZE*EXAMPLES_SIZE
