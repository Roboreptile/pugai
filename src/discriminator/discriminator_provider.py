from enum import Enum

import tensorflow as tf
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
from src.const import *
from tensorflow.keras import layers

class DiscriminatorType(Enum):
    MOCK = 1
    V1 = 2
    V2 = 3
    V3 = 4
    FINAL = 5

class DiscriminatorProvider:
    
    discriminator:tf.keras.Sequential
    cross_entropy = tf.keras.losses.BinaryCrossentropy(from_logits=True)
    discriminator_optimizer = tf.keras.optimizers.Adam(1e-4)
    
    def __init__(self,type:DiscriminatorType):
        self._initialize_discriminator(type)
        self._test_discriminator()
    
    def loss(self,real_output, fake_output):
        real_loss = self.cross_entropy(tf.ones_like(real_output), real_output)
        fake_loss = self.cross_entropy(tf.zeros_like(fake_output), fake_output)
        total_loss = real_loss + fake_loss
        tf.print(total_loss)
        return total_loss
    
    def get(self):
        return self.discriminator
        
    def apply_gradients(self,gradients_of_discriminator):
        self.discriminator_optimizer.apply_gradients(zip(gradients_of_discriminator, self.discriminator.trainable_variables))
        
    def _initialize_discriminator(self,type:DiscriminatorType):
        if(type is DiscriminatorType.MOCK):
            self.discriminator = self._make_mock()
        elif(type is DiscriminatorType.V1):
            self.discriminator = self._make_V1()
        elif(type is DiscriminatorType.V2):
            self.discriminator = self._make_V2()
        elif(type is DiscriminatorType.V3):
            self.discriminator = self._make_V3()
        elif(type is DiscriminatorType.FINAL):
            self.discriminator = self._make_final()
            
        self.discriminator.summary()
        
    def _test_discriminator(self):
        noise = tf.random.normal([1, IMAGE_SIZE, IMAGE_SIZE, 3])
        decision = self.discriminator(noise, training=False)
        print(decision)

    def _make_final(self):
        model = tf.keras.Sequential()

        model.add(layers.Reshape((IMAGE_SIZE,IMAGE_SIZE,3), input_shape=(IMAGE_SIZE,IMAGE_SIZE,3)))
        model.add(layers.Conv2D(16, kernel_size=(5,5), strides=(1,1), use_bias=False))        
        model.add(layers.Conv2D(8,  kernel_size=(3,3), strides=(1,1), use_bias=False))        
        model.add(layers.Conv2D(4,  kernel_size=(3,3), strides=(1,1), use_bias=False))
        model.add(layers.MaxPooling2D(pool_size=(2,2), strides=(2,2),padding='valid'))

        model.add(layers.Flatten())
        model.add(layers.Dense(512,activation='relu'))
        model.add(layers.Dense(512,activation='relu'))
        model.add(layers.Dense(512,activation='relu'))
    
        model.add(layers.Dense(1))
        
        return model

    ## ======================= THESE WERE USED BEFORE =========================================
    def _make_mock(self):
        model = tf.keras.Sequential()

        model.add(layers.Reshape((IMAGE_SIZE,IMAGE_SIZE,3,1), input_shape=(IMAGE_SIZE,IMAGE_SIZE,3)))
        model.add(layers.MaxPool3D((IMAGE_SIZE,IMAGE_SIZE,3),strides=(1,1,1), padding='valid'))
        model.add(layers.Flatten())
        model.add(layers.Dense(1))
        
        return model

    def _make_V1(self):
        model = tf.keras.Sequential()

        model.add(layers.Reshape((IMAGE_SIZE,IMAGE_SIZE,3), input_shape=(IMAGE_SIZE,IMAGE_SIZE,3)))
        model.add(layers.Conv2D(16, kernel_size=(5,5), strides=(1,1), use_bias=False))        
        model.add(layers.Conv2D(8,  kernel_size=(3,3), strides=(1,1), use_bias=False))        
        model.add(layers.Conv2D(4,  kernel_size=(3,3), strides=(1,1), use_bias=False))
        model.add(layers.MaxPooling2D(pool_size=(2,2), strides=(2,2),padding='valid'))

        model.add(layers.Flatten())
        model.add(layers.Dense(512,activation='relu'))
        model.add(layers.Dense(512,activation='relu'))
        model.add(layers.Dense(512,activation='relu'))
    
        model.add(layers.Dense(1))
        
        return model
    
    def _make_V2(self):
        model = tf.keras.Sequential()

        model.add(layers.Reshape((IMAGE_SIZE,IMAGE_SIZE,3), input_shape=(IMAGE_SIZE,IMAGE_SIZE,3)))
        model.add(layers.Conv2D(32, kernel_size=(5,5), strides=(1,1), use_bias=False))        
        model.add(layers.Conv2D(16, kernel_size=(3,3), strides=(1,1), use_bias=False))        
        model.add(layers.Conv2D(8,  kernel_size=(3,3), strides=(1,1), use_bias=False))

        model.add(layers.MaxPooling2D(pool_size=(2,2), strides=(2,2),padding='valid'))

        model.add(layers.Flatten())
        model.add(layers.Dense(1024,activation='relu'))
        model.add(layers.Dense(1024,activation='relu'))
        model.add(layers.Dense(1024,activation='relu'))
    
        model.add(layers.Dense(1))
        
        return model

    def _make_V3(self):
        model = tf.keras.Sequential()

        model.add(layers.Reshape((IMAGE_SIZE,IMAGE_SIZE,3), input_shape=(IMAGE_SIZE,IMAGE_SIZE,3)))
        model.add(layers.Conv2D(16, kernel_size=(5,5), strides=(1,1), use_bias=False))
        model.add(layers.MaxPooling2D(pool_size=(2,2), strides=(2,2),padding='valid'))
        model.add(layers.Conv2D(16, kernel_size=(5,5), strides=(1,1), use_bias=False))
        model.add(layers.MaxPooling2D(pool_size=(2,2), strides=(2,2),padding='valid'))

        model.add(layers.Flatten())
        model.add(layers.Dense(512,activation='relu'))
        model.add(layers.Dense(512,activation='relu'))
        model.add(layers.Dense(512,activation='relu'))
        model.add(layers.Dense(1))
        
        return model
   