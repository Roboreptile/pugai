import tensorflow as tf
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
import glob
import imageio
import matplotlib.pyplot as plt
import time

from IPython import display
from tensorflow.python.data.ops.dataset_ops import BatchDataset

from src.generator_provider import *
from src.discriminator_provider import *
from src.data_provider import *
from src.const import *

class GANProvider:
    
    generator_provider:GeneratorProvider
    discriminator_provider:DiscriminatorProvider
    data_provider:DataProvider
    
    generator_model:tf.keras.Sequential
    discriminator_model:tf.keras.Sequential
    checkpoint:tf.train.Checkpoint
    
    data_in: BatchDataset
    data_out: BatchDataset
    
    seed_noise = tf.random.normal([GIF_SIZE_SQUARED, IMAGE_SIZE*IMAGE_SIZE*3])
    seed_dogs = tf.random.normal([GIF_SIZE_SQUARED, IMAGE_SIZE*IMAGE_SIZE*3])

    def __init__(self,generator:GeneratorProvider,discriminator:DiscriminatorProvider, data:DataProvider):
        self.generator_provider = generator
        self.discriminator_provider = discriminator
        self.data_provider = data
        
        self.generator_model = generator.get()
        self.discriminator_model = discriminator.get()

        self.checkpoint = tf.train.Checkpoint(
            generator_optimizer=generator.generator_optimizer,
            discriminator_optimizer=discriminator.discriminator_optimizer,
            generator=generator.get(),
            discriminator=discriminator.get()
        )
    
    def load_data(self):
        s = time.time()
        self.data_in, self.data_out, self.seed_dogs = self.data_provider.load_data()
        s = time.time() - s
        print("Total time to load images: "+str(round(s,2))+" seconds ("+str(round(s/60,2))+" minutes)")
        

    def train(self):
        print("Training started.")

        for epoch in range(EPOCHS):
            start = time.time()

            for image_batch_in, image_batch_out in zip(self.data_in,self.data_out):
                self._train_step(image_batch_in,image_batch_out)

            display.clear_output(wait=True)
            self._generate_and_save_images(epoch + 1)

            if (epoch + 1) % SAVE_EVERY_X_EPOCHS == 0:
                #self.checkpoint.save(file_prefix = CHECKPOINT_PREFIX+'_'+str(epoch+1))
                #print("Checkpoint saved as: "+CHECKPOINT_PREFIX+'_'+str(epoch+1))
                self.generator_model.save(GENERATOR_PREFIX+str(epoch+1)+".h5")
                print("Generator saved as: "+GENERATOR_PREFIX+str(epoch+1))
            print ('Time for epoch {} is {} sec'.format(epoch + 1, time.time()-start))

        self._generate_and_save_images(EPOCHS)
        self.generator_model.save(GENERATOR_PREFIX+"final.h5")

    def create_gif(self):
        anim_file = GIF_NAME+"_noisy.gif"
        with imageio.get_writer(anim_file, mode='I') as writer:
            filenames = glob.glob('./test/noisy/image*.png')
            filenames = sorted(filenames)
            for filename in filenames:
                image = imageio.imread(filename)
                writer.append_data(image)

        anim_file = GIF_NAME+"_doggy.gif"       
        with imageio.get_writer(anim_file, mode='I') as writer:
            filenames = glob.glob('./test/doggy/image*.png')
            filenames = sorted(filenames)
            for filename in filenames:
                image = imageio.imread(filename)
                writer.append_data(image)

                
    @tf.function
    def _train_step(self, i_in, i_out):
        
        with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape:
            generated_images = self.generator_model(i_in, training=True)

            real_output = self.discriminator_model(i_out, training=True)
            fake_output = self.discriminator_model(generated_images, training=True)

            gen_loss = self.generator_provider.loss(fake_output,i_in,generated_images)
            disc_loss = self.discriminator_provider.loss(real_output, fake_output)
            tf.print("===")
            gradients_of_generator = gen_tape.gradient(gen_loss, self.generator_model.trainable_variables)
            gradients_of_discriminator = disc_tape.gradient(disc_loss, self.discriminator_model.trainable_variables)

        self.generator_provider.apply_gradients(gradients_of_generator)
        self.discriminator_provider.apply_gradients(gradients_of_discriminator)
    

    def _generate_and_save_images(self, epoch):

        predictions = self.generator_model(self.seed_noise, training=False)

        fig = plt.figure(figsize=(4, 4))

        for i in range(predictions.shape[0]):
            plt.subplot(4, 4, i+1)
            plt.imshow((predictions[i, :, :, :]+1)/2)
            plt.axis('off')

        plt.savefig('./test/noisy/image_at_epoch_{:04d}.png'.format(epoch))
        plt.close(fig)

        predictions = self.generator_model(self.seed_dogs, training=False)
        fig = plt.figure(figsize=(4, 4))

        for i in range(predictions.shape[0]):
            plt.subplot(4, 4, i+1)
            plt.imshow((predictions[i, :, :, :]+1)/2)
            plt.axis('off')

        plt.savefig('./test/doggy/image_at_epoch_{:04d}.png'.format(epoch))
        plt.close(fig)
