import tensorflow as tf
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
import matplotlib.pyplot as plt
import numpy as np
import os
import PIL
from random import randint

from enum import Enum

from src.const import *


class DataType(Enum):
    ALL = 1
    PUG = 2
    NOISE = 3
    SUPER_ALL = 4
    SUPER_PUG = 5
    
class DataProvider:
    data_type_in:DataType
    data_type_out:DataType

    dataset_in:tf.data.Dataset
    dataset_out:tf.data.Dataset
    
    def __init__(self, data_in:DataType, data_out:DataType, reduce_images_in = 1, reduce_images_out = 1, set_batch = 0, set_buffer = 0, cheat=False):
        self.data_type_in = data_in
        self.data_type_out = data_out

        self.cut_in = reduce_images_in
        self.cut_out = reduce_images_out

        self.set_batch = set_batch
        self.set_buufer = set_buffer
        self.cheat = cheat
    

    def load_data_stochastic(self):
        data_in:np.array
        
        print("Input images - ",end='')
        if(self.data_type_in is DataType.ALL):
            data_in = self._load_all_data()
        elif(self.data_type_in is DataType.PUG):
            data_in = self._load_pug_data()
        elif(self.data_type_in is DataType.SUPER_ALL):
            data_in = self._load_super_all_data(self.cut_in)
        elif(self.data_type_in is DataType.SUPER_PUG):
            data_in = self._load_super_pug_data(self.cut_in)  
        else:
            data_in = self._load_noise_data()
        
        self._show_example_data(data_in,'input')

        data_out:np.array
        print("Output images - ",end='')
        if(self.data_type_out is DataType.ALL):
            data_out = self._load_all_data()
        elif(self.data_type_out is DataType.PUG):
            data_out = self._load_pug_data()
        elif(self.data_type_out is DataType.SUPER_ALL):
            data_out = self._load_super_all_data(self.cut_out) 
        elif(self.data_type_out is DataType.SUPER_PUG):
            data_out = self._load_super_pug_data(self.cut_out)  
        else:
            data_out = self._load_noise_data()

        self._show_example_data(data_in,'input')
        self._show_example_data(data_out,'output')
        print("Example images are now available.")

        train_images_in = (data_in - 127.5)/127.5
        train_images_out =(data_out - 127.5)/127.5
        self.dataset_in = tf.data.Dataset.from_tensor_slices(train_images_in ).shuffle(1809, reshuffle_each_iteration=True).batch(1, drop_remainder=True)
        self.dataset_out= tf.data.Dataset.from_tensor_slices(train_images_out).shuffle(1809,reshuffle_each_iteration=True).batch(1, drop_remainder=True)
        
        example_dogs = train_images_in[np.random.choice(train_images_in.shape[0], GIF_SIZE_SQUARED, replace=True)]
        example_pugs = train_images_out[np.random.choice(train_images_out.shape[0], GIF_SIZE_SQUARED, replace=True)]

        print("Batches per epoch:" + str(len(self.dataset_in)))
        return self.dataset_in, self.dataset_out, example_dogs, example_pugs

    def load_data(self):
        data_in:np.array
        
        print("Input images - ",end='')
        if(self.data_type_in is DataType.ALL):
            data_in = self._load_all_data()
        elif(self.data_type_in is DataType.PUG):
            data_in = self._load_pug_data()
        elif(self.data_type_in is DataType.SUPER_ALL):
            data_in = self._load_super_all_data(self.cut_in)
        elif(self.data_type_in is DataType.SUPER_PUG):
            data_in = self._load_super_pug_data(self.cut_in)  
        else:
            data_in = self._load_noise_data()
        
        self._show_example_data(data_in,'input')

        data_out:np.array
        print("Output images - ",end='')
        if(self.data_type_out is DataType.ALL):
            data_out = self._load_all_data()
        elif(self.data_type_out is DataType.PUG):
            data_out = self._load_pug_data()
        elif(self.data_type_out is DataType.SUPER_ALL):
            data_out = self._load_super_all_data(self.cut_out) 
        elif(self.data_type_out is DataType.SUPER_PUG):
            data_out = self._load_super_pug_data(self.cut_out)  
        else:
            data_out = self._load_noise_data()

        self._show_example_data(data_in,'input')
        self._show_example_data(data_out,'output')
        print("Example images are now available.")

        train_images_in = (data_in - 127.5)/127.5
        train_images_out =(data_out - 127.5)/127.5
        self.dataset_in = tf.data.Dataset.from_tensor_slices(train_images_in ).shuffle(BUFFER_SIZE//self.cut_in, reshuffle_each_iteration=True ).batch(BATCH_SIZE//self.batch_cut, drop_remainder=True)
        if not self.cheat:
            self.dataset_out= tf.data.Dataset.from_tensor_slices(train_images_out).shuffle(BUFFER_SIZE//self.cut_out,reshuffle_each_iteration=True).batch(BATCH_SIZE//self.batch_cut, drop_remainder=True)
            self.dataset_out = self.dataset_out.repeat(20)
        else:
            self.dataset_out= tf.data.Dataset.from_tensor_slices(train_images_in).shuffle(BUFFER_SIZE//self.cut_out,reshuffle_each_iteration=True).batch(BATCH_SIZE//self.batch_cut, drop_remainder=True)
            
        example_dogs = train_images_in[np.random.choice(train_images_in.shape[0], GIF_SIZE_SQUARED, replace=True)]
        
        
        #assert(len(self.dataset_in) == len(self.dataset_out))
        print("Batches per epoch:" + str(len(self.dataset_in)))
        return self.dataset_in, self.dataset_out, example_dogs
    
    def _show_example_data(self,data, type):
        fig = plt.figure(figsize=(EXAMPLES_SIZE, EXAMPLES_SIZE))

        for i in range(EXAMPLES_SIZE_SQUARED):
            plt.subplot(EXAMPLES_SIZE, EXAMPLES_SIZE, i+1)
            plt.imshow(data[randint(0,data.shape[0]-1), :, :, :]/255)
            plt.axis('off')

        plt.savefig('./'+EXAMPLES_NAME+'_'+type+'.png')
        plt.close(fig)
            
    def _load_all_data(self):
        all_directory = os.listdir(ALL_DIR)
        directory_files  = ['']*len(all_directory)
        directory_folder = ['']*len(all_directory)
        size = 0
        
        for iterator, dog_folder in enumerate(all_directory):
            if(len(dog_folder.split("."))>1): continue
            directory_files[iterator] = os.listdir(ALL_DIR+dog_folder)
            directory_folder[iterator] = dog_folder
            size += len(directory_files[iterator])
            
        size = size//self.cut
        
        output = np.zeros((size,IMAGE_SIZE,IMAGE_SIZE,3),dtype=np.float32)

        print("Loading started...")
        folders = len(directory_files)
        iterator = 0
        for iterator_f,files in enumerate(directory_files):
            print("\rLoading "+str(iterator_f+1)+"/"+str(folders)+"...",end="")
            for iterator_p, pug in enumerate(files):
                try:
                    img = PIL.Image.open(ALL_DIR+directory_folder[iterator_f]+"/"+pug)
                    scaled_img = img.resize((IMAGE_SIZE,IMAGE_SIZE),PIL.Image.ANTIALIAS)
                    output[iterator,:,:,:] = np.array(scaled_img,dtype=np.float32)
                    iterator += 1
                    if iterator == size: break
                except:
                    print("File error within the following directory: "+directory_folder[iterator_f])
                    print("File causing problems: "+pug)
                    print("Please remove this file before training next time")
            if iterator == size: break
            
        print("\n"+str(folders)+" folders containing "+str(size)+" files loaded!")
        return output
            
    def _load_noise_data(self):
        print("Loading started...")
        output = tf.random.normal([BUFFER_SIZE, 128, 128, 3])
        print(str(BUFFER_SIZE)+" noise images loaded!")
        return output
        
        
    def _load_pug_data(self):
        directory = os.listdir(PUG_DIR)
        size = len(directory)
        output = np.zeros((size,IMAGE_SIZE,IMAGE_SIZE,3),dtype=np.float32)

        print("Loading started...")
        for iterator, pug in enumerate(directory):
            img = PIL.Image.open(PUG_DIR+pug)
            scaled_img = img.resize((IMAGE_SIZE,IMAGE_SIZE),PIL.Image.ANTIALIAS)
            output[iterator,:,:,:] = np.array(scaled_img,dtype=np.float32)
        print(str(size)+" pug images loaded!")
        
        return output

    def _load_super_all_data(self,cut):
        directory = os.listdir(SUPER_DIR)
        size = len(directory)
        size = size//cut
        output = np.zeros((size+1,IMAGE_SIZE,IMAGE_SIZE,3),dtype=np.float32)

        print("Loading started...")
        for iterator, pug in enumerate(directory):
            img = PIL.Image.open(SUPER_DIR+pug)
            scaled_img = img.resize((IMAGE_SIZE,IMAGE_SIZE),PIL.Image.ANTIALIAS)
            output[iterator,:,:,:] = np.array(scaled_img,dtype=np.float32)
            if(iterator==size): break
        print(str(size)+" super images loaded!")
        
        return output

    def _load_super_pug_data(self,cut):
        directory = os.listdir(SUPER_PUG_DIR)
        size = len(directory)
        size = size//cut
        output = np.zeros((size+1,IMAGE_SIZE,IMAGE_SIZE,3),dtype=np.float32)

        
        print("Loading started...")
        for iterator, pug in enumerate(directory):
            img = PIL.Image.open(SUPER_PUG_DIR+pug)
            scaled_img = img.resize((IMAGE_SIZE,IMAGE_SIZE),PIL.Image.ANTIALIAS)
            output[iterator,:,:,:] = np.array(scaled_img,dtype=np.float32)
            if(iterator==size): break
        print(str(size)+" super pug images loaded!")
        
        return output